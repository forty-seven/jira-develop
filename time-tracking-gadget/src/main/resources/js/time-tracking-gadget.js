function getAvailableProject(){
    AJS.$.ajax({
        url: window.location.origin +"/jira/rest/api/2/project",
        success: function(data) {
            var projectDropdown = AJS.$('#availableProject');
            AJS.$.each(data, function(index, value){
                projectDropdown.append('<option value='+value.key+'>'+value.name+'</option>')
            });
        },
        error: function(){
            console.log("ERROR")
        }
    })
}

function getDeveloper(projectKey){
    AJS.$.ajax({
        url: window.location.origin +"/jira/rest/api/2/project/"+projectKey+"/role/10002",
        success: function(data) {
            var people = AJS.$('#people');
            AJS.$.each(data.actors, function(index, value){
                people.append('<div style="width:120px;margin-left: 20px;display: inline-block">'+
                    '<img src="'+value.avatarUrl.replace('xsmall','xlarge')+'"/>'+
                    '<p>'+value.displayName+'</p>'+
                    '</div>')
            });
        },
        error: function(){
            console.log("ERROR")
        }
    })
}

function getWorkLog(issueId){
    AJS.$.ajax({
        url: window.location.origin +"/jira/rest/api/2/issue/"+issueId+"/worklog",
        success: function(data) {
            var worklog = AJS.$('#worklog .view');
            worklog.text(JSON.stringify(data));
        },
        error: function(){
            console.log("ERROR")
        }
    })
}

function getIssues(boardId,sprintId){
    AJS.$.ajax({
        url: window.location.origin +"/jira/rest/agile/1.0/board/"+boardId+"/sprint/+"+sprintId+"/issue",
        success: function(data) {
            console.log(data);
            var i;
            for(i = 0 ; i < data.issues.length; i++){
                AJS.$('#worklog').append('<h4 style="display:block;width: 100%">'+data.issues[i].key +' ' + data.issues[i].fields.summary +'</h4>');
                AJS.$('#worklog').append('<p style="display:block;width: 100%"> Assignee: '+data.issues[i].fields.assignee.displayName +'</p>');
                AJS.$('#worklog').append('<p style="display:block;width: 100%"> Total work log:</p>');

                var j;
                console.log(data.issues[i].fields.worklog.worklogs)

                if(data.issues[i].fields.worklog.worklogs.length > 0) {
                    for(j = 0 ; j < data.issues[i].fields.worklog.worklogs.length; j++){
                        AJS.$('#worklog').append('<p style="display:block;width: 100%">'
                            +data.issues[i].fields.worklog.worklogs[j].author.displayName
                            +': ' + data.issues[i].fields.worklog.worklogs[j].timeSpent
                            +' (' + data.issues[i].fields.worklog.worklogs[j].comment +')'
                            +'</p>');
                    }
                }

                AJS.$('#worklog').append('<br/><br/>');
            }
        },
        error: function(){
            console.log("ERROR")
        }
    })
}

AJS.$(function(){
    console.log("############## Application start ##############")
    getAvailableProject();

    AJS.$('#go').click(function(){
        console.log(AJS.$('#availableProject').val());
        getDeveloper(AJS.$('#availableProject').val());

        var activeSpring = getActiveSprint();
        console.log("ACTIVE SPRINT")
        console.log(activeSpring)
        AJS.$("#activeSprint h3").text(activeSpring.showInfo());

        // Get List issue
        getIssues(1,activeSpring.id);

        // Get worklog of issue
        // getWorkLog('FUNKE-3');
    });
})




