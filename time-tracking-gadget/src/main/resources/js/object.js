function Sprint(sprintData) {
    this.id = sprintData.id;
    this.state = sprintData.state;
    this.name = sprintData.name;
    this.startDate = sprintData.startDate;
    this.endDate = sprintData.endDate;
    this.goal = sprintData.goal;

}

Sprint.prototype.showInfo = function() {
    return this.name + " ("+ this.startDate.substring(0,10) +" to "+ this.endDate.substring(0,10) +")";
};

function getActiveSprint(){
    console.log("Start get sprint info");
    var sprint;
    AJS.$.ajax({
        async: false,
        url: window.location.origin +"/jira/rest/agile/1.0/board/1/sprint",
        success: function(data) {
            console.log(data);
            var i;
            for(i = 0 ; i < data.values.length; i++){
                if(data.values[i].state == 'active'){
                    sprint = new Sprint(data.values[i]);
                    break;
                }
            }
        },
        error: function(){
            console.log("GET SPRINT ERROR")
        }
    });
    console.log("DONE GET SPRINT")
    return sprint;
}