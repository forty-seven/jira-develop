package ut.com.vominh.jira.worklog;

import org.junit.Test;
import com.vominh.jira.worklog.api.MyPluginComponent;
import com.vominh.jira.worklog.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}